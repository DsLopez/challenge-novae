package com.novae.demo.common.dtos;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import static com.novae.demo.business.domain.constants.NovaeDemoConstants.*;

/**
 * Clase encargada de representar el DTO para crear una tarjeta de crédito
 * @author Sergio Murillo
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCreditCardDto {

    @NotNull(message = NOT_NULL_ERROR)
    private String serial;

    @NotEmpty(message = NOT_EMPTY_ERROR)
    private String type;

    @NotNull(message = NOT_NULL_ERROR)
    private Double amount;

    private Boolean isActivated;

    private Boolean isLocked;

    @NotNull(message = NOT_NULL_ERROR)
    private LocalDateTime expiresAt;

    @NotNull(message = NOT_NULL_ERROR)
    private Long customerId;
}
