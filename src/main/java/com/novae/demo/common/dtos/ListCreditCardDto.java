package com.novae.demo.common.dtos;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Clase encargada de representar el DTO para la respuesta a una lista de tarjetas de crédito
 * @author Sergio Murillo
 */
@Data
@Builder
public class ListCreditCardDto {

    private List<CreditCardDto> response;
    private Integer countResults;
}
