package com.novae.demo.common.dtos;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Clase encargada de representar el DTO para una tarjeta de crédito
 * @author Sergio Murillo
 */
@Data
public class CreditCardDto implements Serializable {

    @Id
    private Long id;

    private String serial;

    private String type;

    private Double amount;

    private LocalDateTime activatedAt;

    private LocalDateTime lockedAt;

    private LocalDateTime expiresAt;

    private Boolean isActivated;

    private Boolean isLocked;

    private CustomerDto customer;
}
