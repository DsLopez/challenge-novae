package com.novae.demo.common.dtos;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Clase encargada de representar el DTO para un cliente
 * @author Sergio Murillo
 */
@Data
public class CustomerDto implements Serializable {

    @Id
    private Long id;

    private String identificationType;

    private String identificationNumber;

    private String name;

    private String city;

    private String country;
}
