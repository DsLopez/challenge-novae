package com.novae.demo.common.dtos;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Clase encargada de representar el DTO para actualizar una tarjeta de crédito
 * @author Sergio Murillo
 */
@Data
@Builder
public class UpdateCreditCardDto {

    private Long id;

    private Double amount;

    private Boolean isActivated;

    private Boolean isLocked;
}
