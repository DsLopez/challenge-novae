package com.novae.demo.common.dtos;

import lombok.Data;

/**
 * Clase encargada de representar el DTO para loguear un usuario
 * @author Sergio Murillo
 */
@Data
public class LoginCustomerDto {

    private String username;

    private String password;
}
