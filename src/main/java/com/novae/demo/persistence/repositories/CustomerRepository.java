package com.novae.demo.persistence.repositories;

import com.novae.demo.persistence.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Interfaz encargada de definir las operaciones a la base de datos a la clase Customer
 * @author Sergio Murillo
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value = "select * from customers c where c.identification_number like %:filter% or lower(c.name) like %:filter% limit :max offset :skip", nativeQuery = true)
    Collection<Customer> findAllByIdentificationOrName(@Param("filter") String filter, @Param("skip") Integer skip, @Param("max") Integer max);

    @Query(value = "select * from customers limit :max offset :skip", nativeQuery = true)
    Collection<Customer> findAll(@Param("skip") Integer skip, @Param("max") Integer max);
}
