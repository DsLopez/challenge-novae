package com.novae.demo.persistence.repositories;

import com.novae.demo.persistence.entities.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz encargada de definir las operaciones a la base de datos a la clase CreditCard
 * @author Sergio Murillo
 */
@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {

}
