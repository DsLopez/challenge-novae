package com.novae.demo.persistence.dao.impl;

import com.novae.demo.persistence.dao.ICreditCardDao;
import com.novae.demo.persistence.dao.common.AbstractHibernateDao;
import com.novae.demo.persistence.entities.CreditCard;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Clase encargada de implementar el DAO para la clase CreditCard
 * @author Sergio Murillo
 */
@Repository
public class CreditCardDao extends AbstractHibernateDao<CreditCard> implements ICreditCardDao {

    public CreditCardDao() {
        super();

        setClazz(CreditCard.class);
    }

    @SuppressWarnings("unchecked")
    public List<CreditCard> findAllByTypeOrNumberOrCustomer(String filter, Integer skip, Integer max) {
        return this.getCurrentSession().createQuery("from " + clazz.getName() + " where lower(type) like concat('%',lower(:filter),'%') or serial like concat('%',:filter,'%')")
            .setParameter("filter", filter)
            .setFirstResult(skip)
            .setMaxResults(max)
            .getResultList();
    }
}
