package com.novae.demo.persistence.dao.common;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Clase encargada de implementar las operaciones comunes de los DAO
 * @author Sergio Murillo
 */
@SuppressWarnings("unchecked")
public abstract class AbstractHibernateDao<T extends Serializable> extends AbstractDao<T> implements IOperations<T> {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public T findOne(final Long id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    @Override
    public List<T> findAll(final Integer skip, final Integer max) {
        return getCurrentSession().createQuery("from " + clazz.getName())
                .setFirstResult(skip)
                .setMaxResults(max)
                .getResultList();
    }

    @Override
    public List<T> findFromQuery(final String field, final Object value) {
        Query query = getCurrentSession().createQuery("" +
                "from "+ clazz.getName() +" where "+ field +" like concat('%',:"+field+",'%')");
        query.setParameter(field, value);
        return query.list();
    }

    @Override
    public T findOneFromQuery(final String field, final Object value) {
        Query query = getCurrentSession().createQuery("" +
                "from "+ clazz.getName() +" where "+ field +" = :"+field+"");
        query.setParameter(field, value);
        return (T) query.getSingleResult();
    }

    @Override
    public void create(final T entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(final T entity) {
        getCurrentSession().clear();
        getCurrentSession().update(entity);
    }

    @Override
    public void delete(final T entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public void deleteById(final Long entityId) {
        final T entity = findOne(entityId);
        delete(entity);
    }

    protected Session getCurrentSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        return session;
    }

}