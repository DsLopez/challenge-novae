package com.novae.demo.persistence.dao.common;

import java.io.Serializable;

/**
 * Clase encargada de abstraer las operaciones comúnes de los DAO
 * @author Sergio Murillo
 */
public abstract class AbstractDao<T extends Serializable> implements IOperations<T> {

    protected Class<T> clazz;

    protected final void setClazz(final Class<T> clazzToSet) {
        clazz = clazzToSet;
    }
}
