package com.novae.demo.persistence.dao.common;

import java.io.Serializable;
import java.util.List;

/**
 * Interfaz encargada de definir las operaciones comunes de los DAO
 * @author Sergio Murillo
 */
public interface IOperations<T extends Serializable> {

    T findOne(final Long id);

    List<T> findAll(final Integer skip, final Integer max);

    List<T> findFromQuery(final String field, final Object value);

    T findOneFromQuery(final String field, final Object value);

    void create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    void deleteById(final Long entityId);

}
