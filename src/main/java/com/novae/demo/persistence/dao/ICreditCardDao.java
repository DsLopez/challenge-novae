package com.novae.demo.persistence.dao;

import com.novae.demo.persistence.dao.common.IOperations;
import com.novae.demo.persistence.entities.CreditCard;

import java.util.List;

/**
 * Interfaz encargada de definir el DAO para la clase CreditCard
 * @author Sergio Murillo
 */
public interface ICreditCardDao extends IOperations<CreditCard> {
    List<CreditCard> findAllByTypeOrNumberOrCustomer(String filter, Integer skip, Integer max);
}
