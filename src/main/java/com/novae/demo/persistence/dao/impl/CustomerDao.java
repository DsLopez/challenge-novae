package com.novae.demo.persistence.dao.impl;

import com.novae.demo.persistence.dao.ICustomerDao;
import com.novae.demo.persistence.dao.common.AbstractHibernateDao;
import com.novae.demo.persistence.entities.Customer;
import org.springframework.stereotype.Repository;

/**
 * Clase encargada de implementar el DAO para la clase Customer
 * @author Sergio Murillo
 */
@Repository
public class CustomerDao extends AbstractHibernateDao<Customer> implements ICustomerDao {

    public CustomerDao() {
        super();

        setClazz(Customer.class);
    }

    public Customer findByUsername(String username) {
      return this.findOneFromQuery("username", username);
    }

    public Customer findByIdentificationNumber(String identificationNumber) {
        return this.findOneFromQuery("identificationNumber", identificationNumber);
    }
}
