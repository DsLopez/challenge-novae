package com.novae.demo.persistence.dao;

import com.novae.demo.persistence.dao.common.IOperations;
import com.novae.demo.persistence.entities.Customer;

/**
 * Intefaz encargada de definir el DAO para la clase Customer
 * @author Sergio Murillo
 */
public interface ICustomerDao extends IOperations<Customer> {

    Customer findByUsername(String username);
    Customer findByIdentificationNumber(String identificationNumber);
}
