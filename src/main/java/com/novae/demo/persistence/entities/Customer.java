package com.novae.demo.persistence.entities;

import javax.persistence.*;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.List;

/**
 * Esta clase representará un cliente con sus atributos:
 * - Id
 * - IdentificationType
 * - IdentificationNumber
 * - Name
 * - City
 * - Country
 * - CardId
 * 
 * @author Sergio Murillo
 */
@Data
@Entity
@Table(name = "customers", indexes = {
        @Index(name = "identification_index", columnList="identification_number"),
        @Index(name = "name_index", columnList="name"),
        @Index(name = "multiple_index", columnList="identification_number, name"),
})
@EntityListeners(AuditingEntityListener.class)
public class Customer implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "identification_type", nullable = false, length = 5)
  private String identificationType;

  @Column(name = "identification_number", nullable = false, length = 15, unique = true)
  private String identificationNumber;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "city", nullable = false, length = 50)
  private String city;

  @Column(name = "country", nullable = false, length = 50)
  private String country;

  @Column(name = "username", nullable = false, length = 15, unique = true)
  private String username;

  @Column(name = "password", nullable = false, length = 80)
  private String password;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer")
  private List<CreditCard> cards;
}
