package com.novae.demo.persistence.entities;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Esta clase representará una tarjeta de crédito con sus atributos:
 * - Id
 * - Serial
 * - Type
 * - Amount
 * - ActivatedAt
 * - LockedAt
 * - ExpiresAt
 *
 * @author Sergio Murillo
 */
@Data
@Entity
@Table(name = "credit_cards", indexes = {@Index(name = "serial_index", columnList="serial")})
@EntityListeners(AuditingEntityListener.class)
public class CreditCard implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serial", nullable = false)
    private String serial;

    @Column(name = "type", nullable = false, length = 20)
    private String type;

    @Column(name = "amount", nullable = false)
    private Double amount;

    @Column(name = "activated_at")
    private LocalDateTime activatedAt;

    @Column(name = "locked_at")
    private LocalDateTime lockedAt;

    @Column(name = "expires_at", nullable = false)
    private LocalDateTime expiresAt;

    @Column(name = "is_activated")
    private Boolean isActivated;

    @Column(name = "is_locked")
    private Boolean isLocked;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer")
    private Customer customer;
}
