package com.novae.demo.services;

import com.novae.demo.business.domain.ICreditCardDomain;
import com.novae.demo.common.dtos.CreateCreditCardDto;
import com.novae.demo.common.dtos.CreditCardDto;
import com.novae.demo.common.dtos.ListCreditCardDto;
import com.novae.demo.common.dtos.UpdateCreditCardDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Clase de tipo controlador encargada de publicar la API de tarjeta de crédito
 * @author Sergio Murillo
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/credit-card")
public class CreditCardController {

  @Autowired
  private ICreditCardDomain creditCardDomain;

  @Autowired
  private ModelMapper modelMapper;

  @GetMapping("/find/{id}")
  public CreditCardDto find(@PathVariable final Long id) {
    CreditCardDto dto =  modelMapper.map(creditCardDomain.findOne(id), CreditCardDto.class);
    return dto;
  }

  @GetMapping(value  = {"/find/{skip}/{max}", "/find/{filter}/{skip}/{max}"})
  public ListCreditCardDto find(@PathVariable final Optional<String> filter, @PathVariable final Integer skip, @PathVariable final Integer max) {
    List<CreditCardDto> dtos;

    if (filter.isPresent()) {
      dtos = creditCardDomain.findAllByTypeOrNumberOrCustomer(filter.get(), skip, max)
              .stream()
              .map(creditCard -> modelMapper.map(creditCard, CreditCardDto.class))
              .collect(Collectors.toList());
    } else {
      dtos = creditCardDomain.findAll(skip, max)
              .stream()
              .map(creditCard -> modelMapper.map(creditCard, CreditCardDto.class))
              .collect(Collectors.toList());
    }

    return ListCreditCardDto.builder().response(dtos).countResults(dtos.size()).build();
  }

  @PostMapping(value = "/create")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity create(@Valid @RequestBody final CreateCreditCardDto dto) {
    creditCardDomain.create(dto);
    return ResponseEntity.ok(HttpStatus.CREATED);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity delete(@PathVariable final Long id) {
    creditCardDomain.delete(id);
    return ResponseEntity.ok(HttpStatus.OK);
  }

  @PutMapping(value = "/update")
  public ResponseEntity update(@Valid @RequestBody final UpdateCreditCardDto dto) {
    creditCardDomain.update(dto);
    return ResponseEntity.ok(HttpStatus.OK);
  }
}
