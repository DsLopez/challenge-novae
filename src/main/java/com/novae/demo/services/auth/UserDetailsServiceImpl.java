package com.novae.demo.services.auth;

import com.novae.demo.common.dtos.LoginCustomerDto;
import com.novae.demo.persistence.dao.ICustomerDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

/**
 * Clase encargada de obtener los datos de usuario y contraseña de un cliente a partir de su usuario
 * @author Sergio Murillo
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private ICustomerDao customerDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginCustomerDto applicationUser = modelMapper.map(customerDao.findByUsername(username), LoginCustomerDto.class);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
    }
}