package com.novae.demo.services;

import com.novae.demo.common.dtos.CustomerDto;
import com.novae.demo.common.dtos.ListCustomerDto;
import com.novae.demo.persistence.repositories.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Clase de tipo controlador encargada de publicar la API de clientes
 * @author Sergio Murillo
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/password/{password}")
    public String getPassword(@PathVariable final String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    @GetMapping(value  = {"/find/{skip}/{max}", "/find/{filter}/{skip}/{max}"})
    public ListCustomerDto find(@PathVariable final Optional<String> filter, @PathVariable final Integer skip, @PathVariable final Integer max) {
        List<CustomerDto> dtos;
        if (filter.isPresent()) {
            dtos = customerRepository.findAllByIdentificationOrName(filter.get(), skip, max)
                    .stream()
                    .map(creditCard -> modelMapper.map(creditCard, CustomerDto.class))
                    .collect(Collectors.toList());
        } else {
            dtos = customerRepository.findAll(skip, max)
                    .stream()
                    .map(creditCard -> modelMapper.map(creditCard, CustomerDto.class))
                    .collect(Collectors.toList());
        }

        return ListCustomerDto.builder().response(dtos).countResults(dtos.size()).build();
    }
}
