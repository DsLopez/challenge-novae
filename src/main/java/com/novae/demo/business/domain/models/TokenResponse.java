package com.novae.demo.business.domain.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * Clase encargada de modelar la respuesta del token
 * @author Sergio Murillo
 */
@Data
@Builder
public class TokenResponse {

    private String token;

    @JsonProperty("expiration_at")
    private Date expirationAt;
}
