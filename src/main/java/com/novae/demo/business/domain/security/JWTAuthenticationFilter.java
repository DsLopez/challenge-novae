package com.novae.demo.business.domain.security;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.novae.demo.business.domain.models.TokenResponse;
import com.novae.demo.persistence.entities.Customer;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.novae.demo.business.domain.constants.NovaeDemoConstants.*;

/**
 * Clase encargada de manejar la autenticación con JWT del usuario y retornar el token bearer
 * @author Sergio Murillo
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Metodo encargado de loguear al usuario con sus credenciales
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            Customer creds = new ObjectMapper()
                    .readValue(req.getInputStream(), Customer.class);

            // Se pasan las credenciales al manager
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Metodo encargado de generar el token bearer
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException, JsonMappingException {
        PrintWriter out = res.getWriter();
        Date expiresAt = new Date(System.currentTimeMillis() + EXPIRATION_TIME);

        // Se genera el JWT utilizando la firma
        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(expiresAt)
                .sign(HMAC512(SECRET.getBytes()));

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String tokenJson = ow.writeValueAsString(TokenResponse.builder().token(token).expirationAt(expiresAt).build());

        // Se agrega a la respuesta el token bearer en el body y header
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        out.print(tokenJson);
        out.flush();
    }
}
