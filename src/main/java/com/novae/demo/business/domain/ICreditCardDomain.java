package com.novae.demo.business.domain;

import com.novae.demo.common.dtos.CreateCreditCardDto;
import com.novae.demo.common.dtos.UpdateCreditCardDto;
import com.novae.demo.persistence.entities.CreditCard;

import java.util.List;

/**
 * Interfaz encargada de conectar con el dominio de tarjeta de crédito
 * @author Sergio Murillo
 */
public interface ICreditCardDomain {

    void create(final CreateCreditCardDto dto);

    void update(final UpdateCreditCardDto dto);

    void delete(final Long id);

    List<CreditCard> findAllByTypeOrNumberOrCustomer(final String filter, final Integer skip, final Integer max);

    CreditCard findOne(final Long id);

    List<CreditCard> findAll(final Integer skip, final Integer max);
}
