package com.novae.demo.business.domain.impl;

import com.novae.demo.business.domain.ICreditCardDomain;
import com.novae.demo.business.domain.constants.NovaeDemoConstants;
import com.novae.demo.business.domain.exceptions.InternalServerException;
import com.novae.demo.business.domain.exceptions.ResourceNotFoundException;
import com.novae.demo.business.domain.utils.CreditCardMasker;
import com.novae.demo.business.domain.utils.ResponsesFactory;
import com.novae.demo.common.dtos.UpdateCreditCardDto;
import com.novae.demo.persistence.dao.ICreditCardDao;
import com.novae.demo.persistence.dao.ICustomerDao;
import com.novae.demo.persistence.entities.CreditCard;
import com.novae.demo.common.dtos.CreateCreditCardDto;
import com.novae.demo.persistence.entities.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Clase encargada de manejar lógica de negocio para las tarjetas de crédito
 * @author Sergio Murillo
 */
@Service
@Transactional
public class CreditCardDomain implements ICreditCardDomain {

    @Autowired
    private ICreditCardDao creditCardDao;

    @Autowired
    private ICustomerDao customerDao;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Metodo encargado de crear una tarjeta de crédito
     */
    public void create(final CreateCreditCardDto dto) {
        try {
            Customer customerFound =  customerDao.findOne(dto.getCustomerId());
            if (customerFound != null) {
                CreditCard creditCard = modelMapper.map(dto, CreditCard.class);
                creditCard.setCustomer(customerFound);
                creditCard.setSerial(CreditCardMasker.maskCreditCard(dto.getSerial()));
                creditCard.setActivatedAt(creditCard.getIsActivated() ? LocalDateTime.now() : null);
                creditCard.setLockedAt(creditCard.getIsLocked() ? LocalDateTime.now() : null);
                creditCardDao.create(creditCard);
            } else {
                throw new ResourceNotFoundException(ResponsesFactory.getResourceNotFoundResponse(NovaeDemoConstants.CREDIT_CARD));
            }
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }

    /**
     * Metodo encargado de actualizar una tarjeta de crédito
     */
    public void update(final UpdateCreditCardDto dto) {
        try {
            CreditCard creditCard = creditCardDao.findOne(dto.getId());
            if (creditCard != null) {
                modelMapper.map(dto, creditCard);
                creditCardDao.update(creditCard);
            } else {
                throw new ResourceNotFoundException(ResponsesFactory.getResourceNotFoundResponse(NovaeDemoConstants.CREDIT_CARD));
            }

        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }

    /**
     * Metodo encargado de eliminar una tarjeta de crédito
     */
    public void delete(final Long id) {
        try {
            CreditCard creditCard = creditCardDao.findOne(id);
            if (creditCard != null) {
                creditCardDao.deleteById(id);
            } else {
                throw new ResourceNotFoundException(ResponsesFactory.getResourceNotFoundResponse(NovaeDemoConstants.CREDIT_CARD));
            }
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }

    /**
     * Metodo encargado de buscar una tarjeta de crédito por ID
     */
    public CreditCard findOne(final Long id) {
        try {
            CreditCard creditCard = creditCardDao.findOne(id);
            if (creditCard != null) {
                return creditCard;
            } else {
                throw new ResourceNotFoundException(ResponsesFactory.getResourceNotFoundResponse(NovaeDemoConstants.CREDIT_CARD));
            }
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }

    /**
     * Metodo encargado de buscar tarjetas de crédito por tipo y número
     */
    public List<CreditCard> findAllByTypeOrNumberOrCustomer(final String filter, final Integer skip, final Integer max) {
        try {
            if (max > 200) {
                throw new Exception();
            }
            return creditCardDao.findAllByTypeOrNumberOrCustomer(filter, skip, max);
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }

    /**
     * Metodo encargado de buscar todas las tarjetas de crédito paginadas
     */
    public List<CreditCard> findAll(final Integer skip, final Integer max) {
        try {
            if (max > 200) {
                throw new Exception();
            }
            return creditCardDao.findAll(skip, max);
        } catch (Exception e) {
            throw new InternalServerException(ResponsesFactory.getInternalServerErrorResponse(e.getMessage()));
        }
    }
}
