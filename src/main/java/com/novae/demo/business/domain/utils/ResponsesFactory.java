package com.novae.demo.business.domain.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import static com.novae.demo.business.domain.constants.NovaeDemoConstants.*;

@Data
public class ResponsesFactory {

    public static String getInternalServerErrorResponse(String message) {
        return ResponsesFactory.formatErrorResponse(
                String.format(INTERNAL_SERVER_ERROR_MESSAGE, message));
    }

    public static String getResourceNotFoundResponse(String field) {
        return ResponsesFactory.formatErrorResponse(
                String.format(NOT_FOUND_RESOURCE, field));
    }

    private static String formatErrorResponse(String message) {
        String messageErrorResponse = "";

        try {
            Map<String, Object> errorPayload = new HashMap<>();
            errorPayload.put("errorMessage", message);
            messageErrorResponse = new ObjectMapper().writeValueAsString(errorPayload);
        } catch (JsonProcessingException e) {
            messageErrorResponse = INTERNAL_SERVER_ERROR_MESSAGE;
        }

        return messageErrorResponse;
    }
}
