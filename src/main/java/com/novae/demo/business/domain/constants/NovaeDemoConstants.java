package com.novae.demo.business.domain.constants;

/**
 * Clase encargada de almacenar constantes
 *
 * @author Sergio Murillo
 */
public class NovaeDemoConstants {

    private NovaeDemoConstants() {
        /*
         * Private constructor. Utility class
         */
    }

    public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error [%s]";
    public static final String NOT_FOUND_RESOURCE = "Resource Not Found [%s]";

    public static final String CREDIT_CARD = "Credit Card";

    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    public static final String GET_PASSWORD = "/api/customer/password/{\\d+}";

    public static final String NOT_NULL_ERROR = "El campo no puede ser null";
    public static final String NOT_EMPTY_ERROR = "El campo no puede estar vacio";
    public static final String EQUAL_CHARACTERS_ERROR = "La cantidad de caracteres debe ser de {min}";

    public static final String[] SWAGGER_URLS = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };
}
