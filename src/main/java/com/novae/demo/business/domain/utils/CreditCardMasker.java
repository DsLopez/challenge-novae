package com.novae.demo.business.domain.utils;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCardMasker {

    public static String maskCreditCard(String sentenceThatMightContainACreditCard) {
        String maskedSentence = null;

        // Encontrará find Visa, MasterCard, Amex, Diners, Discovery y JCB.
        Pattern regex = Pattern.compile("\\b(?:4[ -]*(?:\\d[ -]*){11}(?:(?:\\d[ -]*){3})?\\d|"
                + "(?:5[ -]*[1-5](?:[ -]*\\d){2}|(?:2[ -]*){3}[1-9]|(?:2[ -]*){2}[3-9][ -]*"
                + "\\d|2[ -]*[3-6](?:[ -]*\\d){2}|2[ -]*7[ -]*[01][ -]*\\d|2[ -]*7[ -]*2[ -]*0)(?:[ -]*"
                + "\\d){12}|3[ -]*[47](?:[ -]*\\d){13}|3[ -]*(?:0[ -]*[0-5]|[68][ -]*\\d)(?:[ -]*"
                + "\\d){11}|6[ -]*(?:0[ -]*1[ -]*1|5[ -]*\\d[ -]*\\d)(?:[ -]*"
                + "\\d){12}|(?:2[ -]*1[ -]*3[ -]*1|1[ -]*8[ -]*0[ -]*0|3[ -]*5(?:[ -]*"
                + "\\d){3})(?:[ -]*\\d){11})\\b");

        Matcher regexMatcher = regex.matcher(sentenceThatMightContainACreditCard);

        if (regexMatcher.find()) {
            // Una tarjeta de crédito ha sido encontrada
            String creditCard = regexMatcher.group();

            // Elimina espacios y guiones en caso de existir
            String strippedCreditCard = creditCard.replaceAll("[ -]+", "");

            // Se toma una parte del String desde la posición 7
            // Finalizando en las ultimas 4
            String subSectionOfCreditCard = strippedCreditCard.substring(6, strippedCreditCard.length() - 4);

            // Se obtienen los primeros 6 caracteres
            String prefix = strippedCreditCard.substring(0, 6);

            // Se reemplaza la subseccion de la tarjeta de crédito con 'xxx'
            String middle = String.join("", Collections.nCopies(subSectionOfCreditCard.length(), "x"));

            //Se obtienen los primeros 4 caracteres
            String suffix = strippedCreditCard.substring(strippedCreditCard.length() - 4, strippedCreditCard.length());

            // Enmascarar la subsección con 'x'. e.j 378282xxxxx0005
            String maskedCreditCard = prefix + middle + suffix;

            maskedSentence = sentenceThatMightContainACreditCard.replace(creditCard, maskedCreditCard);
        } else {
            maskedSentence = sentenceThatMightContainACreditCard;
        }

        return maskedSentence;
    }
}
