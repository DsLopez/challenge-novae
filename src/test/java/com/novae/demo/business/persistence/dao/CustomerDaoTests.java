package com.novae.demo.business.persistence.dao;

import com.novae.demo.persistence.dao.ICustomerDao;
import com.novae.demo.persistence.entities.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CustomerDaoTests {

    @Autowired
    private ICustomerDao customerDao;

    @Before
    public void init() {
        Customer customer = new Customer();
        customer.setIdentificationType("cc");
        customer.setIdentificationNumber("1120125431");
        customer.setName("Pepito Perez");
        customer.setCity("Bogotá");
        customer.setCountry("Colombia");
        customer.setUsername("test.test");
        customer.setPassword("123456");
        customerDao.create(customer);
    }

    @Test
    @Transactional
    public void assertThatCustomerCanBe() {
        Customer customer = customerDao.findByUsername("test.test");
        assertThat(customer).isNotNull();
        assertThat(customer.getUsername()).isEqualTo("test.test");
    }
}
