package com.novae.demo.business.domain;

import com.novae.demo.business.domain.exceptions.ResourceNotFoundException;
import com.novae.demo.common.dtos.UpdateCreditCardDto;
import com.novae.demo.persistence.dao.ICreditCardDao;
import com.novae.demo.persistence.dao.ICustomerDao;
import com.novae.demo.persistence.entities.CreditCard;
import com.novae.demo.persistence.entities.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CreditCardDomainTests {

    @Autowired
    private ICreditCardDao creditCardDao;

    @Autowired
    private ICustomerDao customerDao;

    @Autowired
    private ICreditCardDomain creditCardDomain;

    @Before
    public void init() {
        CreditCard creditCard = new CreditCard();
        Customer customerOne = new Customer();
        customerOne.setIdentificationType("cc");
        customerOne.setIdentificationNumber("1120125412");
        customerOne.setName("Pepito Perez");
        customerOne.setCity("Bogotá");
        customerOne.setCountry("Colombia");
        customerOne.setUsername("test");
        customerOne.setPassword("123456");
        customerDao.create(customerOne);

        Customer customerFound = customerDao.findByUsername("test");

        creditCard.setCustomer(customerFound);
        creditCard.setType("MasterCard");
        creditCard.setSerial("44412546698");
        creditCard.setAmount(150000.0);
        creditCard.setExpiresAt(LocalDateTime.now());
        creditCardDao.create(creditCard);
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanBeFoundByType() {
        List<CreditCard> creditCards = creditCardDomain.findAllByTypeOrNumberOrCustomer("master", 0, 10);
        assertThat(creditCards).isNotNull();
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanNotBeFoundByType() {
        List<CreditCard> creditCards = creditCardDomain.findAllByTypeOrNumberOrCustomer("other", 0, 10);
        assertThat(creditCards.size()).isEqualTo(0);
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanBeFoundByAll() {
        List<CreditCard> creditCards = creditCardDomain.findAll(0, 10);
        assertThat(creditCards).isNotNull();
        assertThat(creditCards.size()).isGreaterThan(0);
    }

    @Test
    @Transactional
    public void assertThatCreditCardNotCanBeFoundByAll() {
        List<CreditCard> creditCards = creditCardDomain.findAll(0, 0);
        assertThat(creditCards).isNotNull();
        assertThat(creditCards.size()).isEqualTo(0);
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanGetSingle() {
        List<CreditCard> creditCards = creditCardDomain.findAllByTypeOrNumberOrCustomer("master", 0, 10);
        CreditCard creditCardBD = creditCards.get(0);
        CreditCard creditCard = creditCardDomain.findOne(creditCardBD.getId());
        assertThat(creditCards).isNotNull();
        assertThat(creditCard.getType()).isEqualTo("MasterCard");
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanUpdate() {
        List<CreditCard> creditCards = creditCardDomain.findAllByTypeOrNumberOrCustomer("master", 0, 10);
        CreditCard creditCardBD = creditCards.get(0);
        UpdateCreditCardDto update = UpdateCreditCardDto.builder().id(creditCardBD.getId()).amount(1500000.0).build();
        creditCardDomain.update(update);
        assertThat(creditCardBD).isNotNull();
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanNotUpdate() {
        UpdateCreditCardDto update = UpdateCreditCardDto.builder().id(Long.valueOf(9999)).amount(1500000.0).build();
        assertThatExceptionOfType(ResourceNotFoundException.class)
            .isThrownBy(() -> {
                creditCardDomain.update(update);
            });
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanDelete() {
        List<CreditCard> creditCards = creditCardDomain.findAllByTypeOrNumberOrCustomer("master", 0, 10);
        CreditCard creditCardBD = creditCards.get(0);
        creditCardDomain.delete(creditCardBD.getId());
        assertThat(creditCardBD).isNotNull();
    }

    @Test
    @Transactional
    public void assertThatCreditCardCanNotDelete() {
        assertThatExceptionOfType(ResourceNotFoundException.class)
                .isThrownBy(() -> {
                    creditCardDomain.delete(Long.valueOf(9999));
                });
    }
}
